# SDK Contributors

- Paolo Prem  (paolo@chino.io), Chino.io
- Andrea Arighi (andrea@chino.io), Chino.io
- Stefano Tranquillini (stefano.tranquillini@gmail.com), Chino.io

- - -

*Add your info on the bottom of the list. Suggested format:*

    - name, (email), company
