# [COMMUNITY] Chino Swift SDK <!-- omit in toc -->

Swift SDK for Chino.io API

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

_The iOS deployment target is set to 10.3. If you want to change it, 
check the settings of the project under *General* tab._

- [Install](#install)
  - [CocoaPods](#cocoapods)
  - [Carthage](#carthage)
  - [Import directory in Xcode](#import-directory-in-xcode)
- [Test](#test)
- [Usage](#usage)
  - [Example: Create a Repository](#example-create-a-repository)

## Install

You can integrate the CHINO.io Swift SDK into your project 
using one of several methods.

### CocoaPods

1. Install [CocoaPods](http://cocoapods.org):

    $ gem install cocoapods

2. Navigate to the directory that contains your project. 
   Run `pod init`, or open an existing Podfile
   
3. Add the following line to the main loop:
   
        pod 'ChinoOSXLibrary', :git => 'https://gitlab.com/chinoio-public/sdk-chino-swift.git', :tag => '1.3'
    
    Your Podfile should look something like this:

    ```ruby
    use_frameworks!

    target '<YOUR_PROJECT_NAME>' do
    pod 'ChinoOSXLibrary', :git => 'https://gitlab.com/chinoio-public/sdk-chino-swift.git', :tag => '1.3'
    end
    ```

4. Install the dependency:

    ```bash
    $ pod install
    ```

5. Once your project is integrated with the CHINO.io Swift SDK, you can 
   pull SDK updates using the following command:

    ```bash
    $ pod update
    ```

### Carthage
[Carthage](https://github.com/Carthage/Carthage) offers more 
flexibility than CocoaPods, but requires some additional work. 

1. Install Carthage (with Xcode 7+) via [Homebrew](http://brew.sh/):

    ```bash
    brew update
    brew install carthage
    ```

2. Create a `Cartfile` in your project with the following content:

    ```
    # CHINO.io
    github "https://gitlab.com/chinoio-public/sdk-chino-swift.git" ~> 1.3
    ```

3. Install the dependency: this will checkout and build the Swift SDK 
   repository.

    ```bash
    carthage update --platform iOS
    ```

Then, in the Project Navigator in Xcode:
   
1. navigate to **General** > **Linked Frameworks and Libraries**
   
2. drag and drop `ChinoOSXLibrary.framework` (from `Carthage/Build/iOS`).
   
3. Navigate to **Build Phases** > **+** > **New Run Script Phase**. 
   
4. In the newly-created **Run Script** section, add the following 
  code to the script body area (beneath the "Shell" box):
    ```bash
    /usr/local/bin/carthage copy-frameworks
    ```

5. Finally, navigate to the **Input Files** section and add the following path:

    ```bash
    $(SRCROOT)/Carthage/Build/iOS/ChinoOSXLibrary.framework
    ```

### Import directory in Xcode

1. Drag the folder `ChinoOSXLibrary` within the `ChinoOSXLibrary` directory 
   into your Xcode project. It's the folder marked with `<--` in the tree
   structure:

        ChinoOSXLibrary/
            ┬  ChinoOSXLibrary/  <--
            |    └  ...
            ├  CommonCrypto/
            ├  LICENSE
            └  README.md


2. Make sure to check `Copy items if needed`. For `Added Folders` select 
   `Create Groups`.

3. Sometimes you may have libraries which define the same names as some classes 
   in the ChinoOSXLibrary. For example Firebase has also a class called 
   `Auth`. Simply put the name of the imported library in front of 
   the appearing errors.

    **Example:**

    Instead of:
    ```Swift
    import FirebaseAuth
    Auth.auth()
    ```

    write:
    ``` Swift
    import FirebaseAuth
    FirebaseAuth.Auth.auth()
    ```

## Test

There are some tests you can use as examples of usage
of the framework under *ChinoOSXLibraryTests/* folder

Insert your <customer-id> and <customer-key> values in the 
`ChinoOSXLibrary/iOS/api/common/Credentials.swift` file.  
Such credentials will be retrieved when test classes are evaluated.  

## Usage

Once you have successfully imported the *Chino.io* SDKs you have to create 
an instance of `ChinoAPI`

```Swift
import ChinoOSXLibrary

var customerId = "<your-customer-id>"
var customerKey = "<your-customer-key>"
var server = "https://api.test.chino.io/v1"

var chino = ChinoAPI.init(hostUrl: server, customerId: customerId, customerKey: customerKey)
```  

Note that in this example we have used the **test** server. If you want 
to use the the **production** server, use the following server URL: 
`https://api.chino.io/v1`
  
The functions are asyncronous, this means that they are called on 
separate threads while the main thread continues to run.  
  
### Example: Create a Repository

```Swift
chino.repositories.createRepository(description: "test_repository_description") { (response) in

    // Response is a function which returns the requested resource (in this case a Repository) and
    // returns an error if the call fails
    var repository: Repository!
    do{
        repository = try response()
    } catch let error {

        // Here you can manage the error
        print((error as! ChinoError).toString())
    }
    ...
}

// <---- ASYNC: While the call is executed the main thread continues to run

```  
