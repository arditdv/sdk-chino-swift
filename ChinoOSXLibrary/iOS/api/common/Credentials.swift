//
//  Credentials.swift
//  ChinoOSXLibrary
//
//  Created by Chino on 16/05/2017.
//  Copyright © 2017 Chino. All rights reserved.
//

import Foundation

open class Credentials {
    public static let customer_id = "<your-customer-id>"
    public static let customer_key = "<your-customer-key>"
    public static let url = "https://api.test.chino.io/v1"
}
